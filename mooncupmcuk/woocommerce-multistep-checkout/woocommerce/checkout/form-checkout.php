<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $woocommerce;

wc_print_notices();

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout
if (!$checkout->enable_signup && !$checkout->enable_guest_checkout && !is_user_logged_in()) {
    echo apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce'));
    return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters('woocommerce_get_checkout_url', WC()->cart->get_checkout_url());
?>

<form  name="checkout" method="post" class="checkout" action="<?php echo esc_url($get_checkout_url); ?>">
    <div id="wizard"><!---Start of jQuery Wizard -->
        <?php do_action('woocommerce_multistep_checkout_before'); ?>
        <?php if (sizeof($checkout->checkout_fields) > 0) : ?>

            <?php do_action('woocommerce_checkout_before_customer_details'); ?>

            <?php if (WC()->cart->ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>
                <h1 class="title-billing-shipping"><?php _e('Delivery Address', 'woocommerce') ?></h1>
            <?php else: ?>
                <h1><?php echo get_option('wmc_billing_label') ? __(get_option('wmc_billing_label'), 'woocommerce-multistep-checkout') : __('Billing Details', 'woocommerce-multistep-checkout') ?></h1>
            <?php endif; ?>
            <div class="billing-tab-contents">
                <h2>Delivery Address</h2>
                <?php
                $country = new WC_Countries;
                $countries = $country->get_allowed_countries();
                $all_countries = $country->get_countries();
                ?>
                <p class="form-row form-row form-row-first">
                    <label for="billing_country">Country</label>
                    <select id="billing_country" name="billing_country"  class="form-standard">
                    <?php
                    echo '<option value="" selected>Select your delivery country</option>';
                    foreach($countries as $key =>$value) {

                        echo '<option value="'.$key.'">'.$value.'</option>';

                    }
                    echo '<option value="??">Other</option>';

                    ?>
                </select>
                    </p>
                <p class="form-row form-row form-row-last">
                    <label for="billing_country">Other</label>
                <select id="billing_country" name="billing_country" class="form-standard">
                    <option value="" selected>Test Other Country List</option>
                    <?php
                    foreach($all_countries as $key=>$value){

                        if(array_key_exists($key,$countries)){

                        }else{

                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }

                    }
                    ?>
                </select>
                </p>
                <?php
                do_action('woocommerce_checkout_billing');
                ?>
            </div>

            <?php if (!WC()->cart->ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>

                <?php do_action('woocommerce_multistep_checkout_before_shipping'); ?>

                <h1 class="title-shipping"><?php echo get_option('wmc_shipping_label') ? __(get_option('wmc_shipping_label'), 'woocommerce-multistep-checkout') : __('Delivery Address', 'woocommerce-multistep-checkout') ?></h1>
                <div class="shipping-tab-contents">
                   <h2>Shipping Address</h2>
                    <?php do_action('woocommerce_checkout_shipping'); ?>

                    <?php do_action('woocommerce_checkout_after_customer_details'); ?>
                </div>

                <?php do_action('woocommerce_multistep_checkout_after_shipping'); ?>
            <?php endif; ?>



        <?php endif; ?>

         <?php do_action('woocommerce_multistep_checkout_before_order_info'); ?>


        <?php if (get_option('wmc_merge_order_payment_tabs') != "true" || get_option('wmc_merge_order_payment_tabs') == ""): ?>
        <h1 class="title-order-info"><?php echo get_option('wmc_orderinfo_label') ? __(get_option('wmc_orderinfo_label'), 'woocommerce-multistep-checkout') : __('Order Information2', 'woocommerce-multistep-checkout'); ?></h1>

            <div class="shipping-tab">
                <h2>Order Confirmation</h2>
                    <?php do_action('woocommerce_checkout_simple'); ?>
            <?php do_action('woocommerce_multistep_checkout_before_order_contents'); ?>

        </div>
        <?php endif?>

        <?php do_action('woocommerce_multistep_checkout_after_order_info'); ?>
        <?php do_action('woocommerce_multistep_checkout_before_payment'); ?>

        <h1 class="title-payment"><?php echo get_option('wmc_paymentinfo_label') ? __(get_option('wmc_paymentinfo_label'), 'woocommerce-multistep-checkout') : __('Payment2', 'woocommerce-multistep-checkout'); ?></h1>
        <div class="payment-tab-contents">
            <h2>Payment</h2>
            <div id="order_review" class="woocommerce-checkout-review-order">
                <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>
                <?php do_action('woocommerce_checkout_order_review'); ?>
            </div>
        </div>


        <?php do_action('woocommerce_multistep_checkout_after'); ?>
    </div>
</form>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>
