<div id="affwp-affiliate-dashboard-url-generator" class="affwp-tab-content">

	<h4><?php _e( 'Welcome to the Mooncup Affiliate Programme', 'affiliatewp' ); ?></h4>

	<?php if ( 'id' == affwp_get_referral_format() ) : ?>
		<p><?php printf( __( 'Your affiliate ID is: <strong>%s</strong>', 'affiliatewp' ), affwp_get_affiliate_id() ); ?></p>
	<?php elseif ( 'username' == affwp_get_referral_format() ) : ?>
		<p><?php printf( __( 'Your affiliate username is: <strong>%s</strong>', 'affiliatewp' ), affwp_get_affiliate_username() ); ?></p>
	<?php endif; ?>

	<p><?php printf( __( 'Your unique affiliate link is: <strong>%s</strong>', 'affiliatewp' ), esc_url( affwp_get_affiliate_referral_url() ) ); ?>
	<br />Share this link with friends and on your blogs and posts. Your link will send your visitors to the home page of our website.</p>



	<form id="affwp-generate-ref-url" class="affwp-form" method="get" action="#affwp-generate-ref-url">

		<div class="affwp-wrap affwp-base-url-wrap">
			<p><?php _e( 'To set up a campaign to send your visitors to a different page copy the URL from that page into the box below:', 'affiliatewp' ); ?><br />
				<span style="font-size: small">For example, entering <em><?php echo esc_url( affwp_get_affiliate_base_url() ); ?>buy-the-mooncup/</em> will direct your visitors directly to buying the Mooncup page.</span><br />
			<input type="text" name="url" id="affwp-url" value="<?php //echo esc_url( affwp_get_affiliate_base_url() ); ?>" placeholder="Paste or type link here"/>
			</p>
		</div>
		<div class="affwp-wrap affwp-campaign-wrap">
			<p>Add a name for this campaign and click the Generate Your Link button.<br />
			<input type="text" name="campaign" id="affwp-campaign" value="" required placeholder="Enter the name for your campaign here" />
			</p>
		</div>

		<div class="affwp-wrap affwp-referral-url-wrap" <?php if ( ! isset( $_GET['url'] ) ) { echo 'style="display:none;"'; } ?>>

			<label for="affwp-referral-url"><?php _e( 'Your link', 'affiliatewp' ); ?></label>
			<input type="text" id="affwp-referral-url" value="<?php echo esc_url( affwp_get_affiliate_referral_url() ); ?>" />
			<div class="description"><?php _e( 'Copy and save the new link before sharing as you will not be able to view it here later. Clicks and sales through this link will appear under your chosen campaign name in Statistics.', 'affiliatewp' ); ?></div>
		</div>

		<div class="affwp-referral-url-submit-wrap">
			<input type="hidden" class="affwp-affiliate-id" value="<?php echo esc_attr( affwp_get_referral_format_value() ); ?>" />
			<input type="hidden" class="affwp-referral-var" value="<?php echo esc_attr( affiliate_wp()->tracking->get_referral_var() ); ?>" />
			<input type="submit" class="button" value="<?php _e( 'Generate Your Link', 'affiliatewp' ); ?>" />
		</div>
	</form>
	<p></p>
</div>
