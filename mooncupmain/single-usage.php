<?php
/**
 * Mooncup Main template for displaying Single-Posts
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */

get_header(); ?>



	<section class="using-your-mooncup using-mooncup-item page-content primary" role="main">
		
	        <article class="container_full splash-content-block">
	        	<?php if (has_post_thumbnail( $post->ID ) ): ?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php echo $image[0]; ?>');">
		        <?php endif; ?>
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        <section class="container_boxed content_band">
	        	<aside class="sidebar col__4">
	        		<ul class="sidebar"><?php
						if ( function_exists( 'dynamic_sidebar' ) ) :
							dynamic_sidebar( 'faq-sidebar' );
						endif; ?>
					</ul>	
	        	</aside>

	        	<article class="faq-content faq-content-item col__8">
	        		<div class="container_boxed breadcrumb-container">
				    	<?php if ( function_exists('yoast_breadcrumb') ) 
						{yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				    </div>

				    <div class="faq-item-post faq-item-listing">
				    <?php
						if ( have_posts() ) : the_post();

							get_template_part( 'loop', get_post_format() ); ?>

							<aside class="post-aside">

								<div class="post-links">
									<?php previous_post_link( '%link', __( '&laquo; Previous post', 'mooncupmain' ) ) ?>
									<?php next_post_link( '%link', __( 'Next post &raquo;', 'mooncupmain' ) ); ?>
								</div>

							</aside><?php

						else :

							get_template_part( 'loop', 'empty' );

						endif;
					?>
					
					</div>

				<div class="link-container caps-text center">
					<a href="#">BACK TO ALL QUESTIONS</a>
				</div>

	        	</article>

	        </section>
	
</section>
<?php get_footer(); ?>