<?php
/**
 * Mooncup Main template for displaying Pages
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 */

get_header(); ?>
	<section class="shop-mooncup page-content primary" role="main">
		<div class="container_boxed content_band">

			<?php woocommerce_content(); ?>
		</div>
	</section>

<?php get_footer(); ?>
